package idento.semaphore.regles;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JesUtils {
	public static void verifyFile(String file) {
		File tempFile = new File(file);
		boolean exists = tempFile.exists();

		if (exists) {
			System.out.println("Exist");
		}
		else
		{
			try (FileWriter writer = new FileWriter(new File(file))) {
				writer.append(";;;;;;;;;;;");
				writer.append("\n");
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
