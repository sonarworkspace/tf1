package idento.semaphore.regles;

import sailpoint.object.*;

import sailpoint.api.IdentityRequestProvisioningScanner;
import sailpoint.task.IdentityRequestMaintenance;
import sailpoint.workflow.WorkflowContext;
import sailpoint.tools.Util;
import sailpoint.web.util.IdentityRequestSummarizer;
import java.util.*;
import java.text.*;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.apache.log4j.Logger; 
import org.apache.log4j.Level; 


public class Traitement {
	
	/*** Génération du GUID ***/
	
	public static String Get_GUID()
	{ 
		UUID uuid = UUID.randomUUID();
		String chaine = uuid.toString().toUpperCase();
		return chaine;
	}
	
	/*** Génération du GUID From AD ***/
	
	public static String Get_GUIDFromAD(String GUID)
	{ 
		String chaine ="";
		chaine = "{" + GUID + "}";	
		return chaine;
	}
	
	/*** Traitement du nom ***/
	
	public static String Traite_Nom(String nom)
	{ 
		String chaine ="";
		if (nom != null)
		{
		 chaine = nom.toUpperCase();
		}
		return chaine;
	}
	
	/*** Traitement du prenom ***/
	
	public static String Traite_Prenom(String prenom)
	{ 
		String chaine ="";
		if (prenom != null)
		{
		 chaine = prenom.toLowerCase();
		 chaine = chaine.substring(0,1).toUpperCase()+chaine.substring(1);
		}
		return chaine;
	}
	
	/*** Calcul du DisplayName ***/
	
	public static String Get_DisplayName(String nom,String prenom)
	{ 
		String chaine ="";
		
		if (nom != null && prenom != null)
		{
			nom =  idento.semaphore.regles.Traitement.Traite_Nom(nom);
			prenom = idento.semaphore.regles.Traitement.Traite_Prenom(prenom);
			chaine = prenom + " "+ nom;
		}
		return chaine;
	}
	
	/*** Calcul du Libelle Domaine ***/
	
	public static String Get_LibelleDomaine()
	{ String chaine ="TF1";
	  return chaine;
	}
	
	/*** Initialisation de données par la valeur "N" ***/
	
	public static String Return_Non()
	{
		String chaine ="N";
		return chaine;
	}
	
	/*** Initialisation de données par la valeur "O" ***/
	
	public static String Return_Oui()
	{
		String chaine ="O";
		return chaine;
	}
	
	/*** Calculer de l'empreinte ***/
	
	public static String Get_Empreinte(String nom, String prenom, String dateNaissance)
	{
		String chaine ="";
		chaine = nom + prenom + dateNaissance;
		return chaine;
	}
	
	/*** Nettoyage des caractères accentués et spéciaux de la chaine ***/
	
	public static String TraiteChaine(String chaine)
	{
		String str;
		
		if(chaine == null) 
		{
			str = "";
		}
		else
		{
		str = chaine.toLowerCase();
		
		str = str.replace("A","A");
		str = str.replace("À","A");
		str = str.replace("Á","A");
		str = str.replace("Â","A");
		str = str.replace("Ã","A");
		str = str.replace("Ä","A");
		str = str.replace("Å","A");
		str = str.replace("Æ","AE");
		str = str.replace("B","B");
		str = str.replace("C","C");
		str = str.replace("Ç","C");
		str = str.replace("D","D");
		str = str.replace("E","E");
		str = str.replace("È","E");
		str = str.replace("É","E");
		str = str.replace("Ê","E");
		str = str.replace("Ë","E");
		str = str.replace("H","H");
		str = str.replace("I","I");
		str = str.replace("Ì","I");
		str = str.replace("Í","I");
		str = str.replace("Î","I");
		str = str.replace("Ï","I");
		str = str.replace("F","F");
		str = str.replace("G","G");
		str = str.replace("J","J");
		str = str.replace("K","K");
		str = str.replace("L","L");
		str = str.replace("M","M");
		str = str.replace("N","N");
		str = str.replace("Ñ","N");
		str = str.replace("O","O");
		str = str.replace("Ò","O");
		str = str.replace("Ó","O");
		str = str.replace("Ô","O");
		str = str.replace("Õ","O");
		str = str.replace("Ö","O");
		str = str.replace("Ø","O");
		str = str.replace("P","P");
		str = str.replace("Q","Q");
		str = str.replace("R","R");
		str = str.replace("S","S");
		str = str.replace("T","T");
		str = str.replace("U","U");
		str = str.replace("Ù","U");
		str = str.replace("Ú","U");
		str = str.replace("Û","U");
		str = str.replace("Ü","U");
		str = str.replace("V","V");
		str = str.replace("W","W");
		str = str.replace("X","X");
		str = str.replace("Y","Y");
		str = str.replace("Ý","Y");
		str = str.replace("Z","Z");
		str = str.replace("a","a");
		str = str.replace("à","a");
		str = str.replace("á","a");
		str = str.replace("â","a");
		str = str.replace("ã","a");
		str = str.replace("ä","a");
		str = str.replace("å","a");
		str = str.replace("æ","ae");
		str = str.replace("b","b");
		str = str.replace("c","c");
		str = str.replace("ç","c");
		str = str.replace("d","d");
		str = str.replace("e","e");
		str = str.replace("è","e");
		str = str.replace("é","e");
		str = str.replace("ê","e");
		str = str.replace("ë","e");
		str = str.replace("f","f");
		str = str.replace("g","g");
		str = str.replace("h","h");
		str = str.replace("i","i");
		str = str.replace("ì","i");
		str = str.replace("í","i");
		str = str.replace("î","i");
		str = str.replace("ï","i");
		str = str.replace("j","j");
		str = str.replace("k","k");
		str = str.replace("l","l");
		str = str.replace("m","m");
		str = str.replace("n","n");
		str = str.replace("ñ","n");
		str = str.replace("o","o");
		str = str.replace("ò","o");
		str = str.replace("ó","o");
		str = str.replace("ô","o");
		str = str.replace("õ","o");
		str = str.replace("ö","o");
		str = str.replace("ø","o");
		str = str.replace("p","p");
		str = str.replace("q","q");
		str = str.replace("r","r");
		str = str.replace("s","s");
		str = str.replace("ß","s");
		str = str.replace("t","t");
		str = str.replace("u","u");
		str = str.replace("ù","u");
		str = str.replace("ú","u");
		str = str.replace("û","u");
		str = str.replace("ü","u");
		str = str.replace("v","v");
		str = str.replace("w","w");
		str = str.replace("x","x");
		str = str.replace("y","y");
		str = str.replace("ý","y");
		str = str.replace("ÿ","y");
		str = str.replace("z","z");
		str = str.replace("!","");
		str = str.replace("#","");
		str = str.replace("$","");
		str = str.replace("%","");
		str = str.replace("&","");
		str = str.replace("'","");
		str = str.replace("(","");
		str = str.replace(")","");
		str = str.replace("*","");
		str = str.replace("+","");
		str = str.replace(",","");
		str = str.replace("-","");
		str = str.replace(".","");
		str = str.replace("/","");
		str = str.replace(":","");
		str = str.replace(";","");
		str = str.replace("<","");
		str = str.replace("=","");
		str = str.replace(">","");
		str = str.replace("?","");
		str = str.replace("@","");
		str = str.replace(" ","");
		str = str.replace("¡","");
		str = str.replace("¢","");
		str = str.replace("£","");
		str = str.replace("¤","");
		str = str.replace("¥","");
		str = str.replace("¦","");
		str = str.replace("§","");
		str = str.replace("¨","");
		str = str.replace("©","");
		str = str.replace("ª","");
		str = str.replace("«","");
		str = str.replace("¬","");
		str = str.replace("­","");
		str = str.replace("®","");
		str = str.replace("¯","");
		str = str.replace("°","");
		str = str.replace("±","");
		str = str.replace("²","");
		str = str.replace("³","");
		str = str.replace("´","");
		str = str.replace("µ","");
		str = str.replace("¶","");
		str = str.replace("·","");
		str = str.replace("¸","");
		str = str.replace("¹","");
		str = str.replace("º","");
		str = str.replace("»","");
		str = str.replace("¼","");
		str = str.replace("½","");
		str = str.replace("¾","");
		str = str.replace("¿","");
		str = str.replace("]","");
		str = str.replace("^","");
		str = str.replace("_","");
		str = str.replace("`","");
		str = str.replace(" ","");
		
		str = str.toUpperCase();
		}

		return str;
	}
	
	/*** Traitement Date ***/
	
	public static String TraiteDate (String chaine)
	{
		String str = chaine;
		
		try {
			
			if (chaine !=null && chaine !="")
			{
				if (chaine.indexOf(" ")!=-1)
				{
					if(chaine.length()>5)
					{
					
					chaine=chaine.substring(0, chaine.indexOf(" ") );

					String jour="";
					String mois="";
					String annee="";
					
					jour = chaine.substring(0, chaine.indexOf("/"));
					chaine= chaine.substring(chaine.indexOf("/")+1, chaine.length());
					mois = chaine.substring(0, chaine.indexOf("/"));
					chaine= chaine.substring(chaine.indexOf("/"), chaine.length());		
					annee= chaine.substring(chaine.indexOf("/")+1, chaine.length());
								
					if (jour.length()==1) {jour = "0"+jour;}
								
					if (mois.length()==1) {mois = "0"+mois;}
								
					//chaine= annee+"-"+mois+"-"+jour; /* Format SandBox */
					chaine= annee+"-"+jour+"-"+mois; /* Format TF1 */
					}

				  }
				
				}
			return chaine;
			
			} 
		catch (Exception e) {
			System.out.println(" *** Exception *** : " + e );
			return str ;
		}
		
	}

	// Convert date format from yyyy-dd-mm to jj/mm/aaaa 
	public static String TraiteDateJES (String chaine)
	{
		String str = chaine;
		
		try {
			
			if (chaine !=null && chaine !="")
			{
					if(chaine.length()>5 && chaine.contains("-"))
					{
					
						//Ancien format aaaa-mm-jj
						String[] chaineSplit = chaine.split("-");
						//nouveau format jj/mm/aaaa
						return chaineSplit[2] + "/" + chaineSplit[1] + "/" + chaineSplit[0];
					}

				
				}
			return "";
			
			} 
		catch (Exception e) {
			System.out.println(" *** Exception *** : " + e );
			return str ;
		}
		
	}	
	
	/*** Détermination du Statut ***/
	
	public static String CalculStatus(String dateFinValidite,String dateDebutValidite, int jour) throws ParseException 
	{
		String statut = "Inactif";
//			if((dateFinValidite==null || dateFinValidite.equals("")))
			if((dateFinValidite==null || dateFinValidite.equals("")) && (dateDebutValidite!=null && !dateDebutValidite.equals("")))
			{
				statut = "Actif";
			}
			else if ((dateFinValidite==null || dateFinValidite.equals("")) && (dateDebutValidite==null || dateDebutValidite.equals(""))){
				statut = "Inactif";
			}
			else
			{
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date dateFin;
				Date dateJour;
				String dateFinTemp;
				String dateJourTemp;
			
				LocalDate dateDuJour = LocalDate.now();
				dateJourTemp = dtf.format(dateDuJour);
				dateJour = sdf.parse(dateJourTemp);
			
				dateFin = sdf.parse(dateFinValidite);
				dateFinTemp = sdf.format(dateFin);
		    
				Calendar cal1 = Calendar.getInstance();
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(dateFin);
		    
				//Nombre de jours à ajouter
				cal2.add(Calendar.DAY_OF_MONTH, jour);  
				//date après ajout du nombre de jours à ajouter
				dateFinTemp  = sdf.format(cal2.getTime()); 
				dateFin = sdf.parse(dateFinTemp );
		 	
				if(dateJour.before(dateFin)) 
				{
					statut= "Actif";
				}
					
			}
				
		return statut;
	}
	
	public static String calculStatusDefauld(String dateFinValidite, String dateDebutValidite) throws ParseException 
	{ 
		return  CalculStatus(dateFinValidite,dateDebutValidite,90); 
	} 
	/*
	public static void main(String... args) {
		String exp = "JEAN -                 LUC ";
		System.out.println(uppercaseFirstLetter(exp));
	}*/
	
	/** 
     * UPPERCASE STRING
     * @param firstname
     * @return
     */
	public static String uppercaseFirstLetter(String firstname) {
		
		String tiret="-", espace=" ";
	
		// PRENOM : Ajout de la gestion des espaces dans le prénom
		if( firstname != null ) {
			firstname = firstname.toLowerCase();
			firstname = firstname.replaceAll(tiret, espace);
			if (firstname.contains(espace)) {
				String [] rawParts= firstname.split(espace);
				List<String> parts = new ArrayList<>();
				for(int i = 0; i< rawParts.length; i++) {
					if(!rawParts[i].trim().isEmpty()) {
						parts.add(rawParts[i]);
					}
				}
				for(int i=0; i< parts.size(); i++) {
					String p = parts.get(i);
					if(i==0) {
						firstname = p.substring(0,1).toUpperCase()+p.substring(1);						
					}else if(i==1) {
						firstname = firstname +espace+ p.substring(0,1).toUpperCase()+p.substring(1);												
					}else {
						firstname = firstname + espace+ p.toLowerCase();						
					}
				}
				
			}else {
				firstname = firstname.substring(0,1).toUpperCase()+firstname.substring(1);
			}
		} else firstname = "";
		return firstname;
	}

	public static String uppercaseletter(String lastname) { 
		 
		if( lastname != null ) { 
		return lastname= lastname.toUpperCase(); 
			 
		}		 
			return"";  
		} 


	
	
/* Calcul de la date de sortie à exporter vers Rhodes */

	public static String export_DateSortie_Rhodes (String codePopulationSAP,String codePopulationRhodes,String dateFinBadge,String dateSortie,String dateMax) 
	{
		int index = 0;
		String codePopulation ="";
	
		/* Vérification de code Population Rhodes, pour extraire les non permanents CPI et les Prestataires */
		if (codePopulationRhodes != null && codePopulationRhodes != "") 
		{
			codePopulation = codePopulationRhodes;	
		}
		/* Vérification de code Population SAP, pour extraire les non permanents*/
		else 
		{
		codePopulation = codePopulationSAP;		
		}
	
		/* DateFinDeBadge Null dans le cube, on prend la date de fin de validité comme DateFinBadge */
	
		if (dateFinBadge == null || dateFinBadge=="" ) 
		{
			dateFinBadge = dateSortie;
		}

		/* Vérification de code Population SAP/Rhodes, pour extraire les non permanents CPI et les prestataires */
	
		if ( codePopulation=="C" || codePopulation=="P" || codePopulation=="I")
		{
			index = 1;
		}
	
	
		if (index == 1) // Cas Externe 
		{
			if (dateFinBadge != "")	// Cas Externe disposant d'une date de fin de badge
			{
				dateSortie =  dateFinBadge;
			}
		
			if (dateFinBadge != null)	// Cas Externe disposant d'une date de fin de badge
			{
				dateSortie =  dateFinBadge;
			}
	  
			else // Cas Externe ne disposant pas d'une date de fin de badge
			{
				dateSortie =  null;
			}
	 
		}
	
		else // Cas Interne
		{

			if (dateFinBadge == null ) // Cas Interne ne disposant pas d'une date de fin de badge
			{
				dateSortie =  null;
			}
	  
			else 
			{
				if (dateFinBadge.compareTo(dateMax)==0) // Cas Interne ne disposant pas d'une date de fin au 31/12/2079
				{
					dateSortie =  null;
				}
				else
				{
					dateSortie = dateFinBadge ;
				}	  
			}
		}

		return dateSortie;
}

	/* Détermination de code population Rhodes à partir de code population SAP */

	public static String calcul_CodePopulation_Rhodes (String codePopulationSAP,String codePopulationRhodes,String contrat) 
	{
	
		String codePopulation=null;
	
		if (codePopulationRhodes != null && codePopulationRhodes != "") 
		{
			codePopulation = codePopulationRhodes;
		}
	
		else 
		{
			if (codePopulationSAP == null || codePopulationSAP=="" ) 
			{
				//codePopulation = "P";
				codePopulation = null;
			}
		
			if (codePopulationSAP=="C" || codePopulationSAP=="P" || codePopulationSAP=="A" || codePopulationSAP=="I"  ) 
			{
				codePopulation = "C";
			}
		
			if (codePopulationSAP=="3") 
			{
				codePopulation = "E";
			}	
		
			if (codePopulationSAP=="1" || codePopulationSAP=="2" ) 
			{
				if (contrat.compareTo("CDI")==0) 
				{
					codePopulation = "S";
				}
			
				if (contrat.compareTo("CDD")==0 || contrat.compareTo("CDD Vacances")==0 ) 
				{
					codePopulation = "CD";
				}
			
				if (contrat.compareTo("Apprentissage")==0 || contrat.compareTo("CDD Profession")==0 || contrat.compareTo("CDD Profession.")==0 ) 
				{
					codePopulation = "CQ";
				}
		
			}
		
		}
	
		return codePopulation;	
	}
	
	/* Evaluation Date Fin Badge avant provisioning dans la base d'échange
	 * Si DateFinValidite > DateFinBadge (vient après), DateFinBadge = DateFinValidite */
	
	public static String evaluate_DateFinBadge (String date_Fin_Validite,String date_Fin_Badge) throws ParseException
	{
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date dateFinValidite;
		Date dateFinBadge;
	    
		if (date_Fin_Validite != null && date_Fin_Validite != "" && date_Fin_Badge !="" && date_Fin_Badge != null) 
		{
			dateFinValidite = sdf.parse(date_Fin_Validite);
			dateFinBadge = sdf.parse(date_Fin_Badge);
	    		
	    
			if(dateFinBadge.before(dateFinValidite)) 
			{
	 		date_Fin_Badge = date_Fin_Validite;
			}
		}
		
		return date_Fin_Badge;
	}
	
	public static Object getSapAttribute(Identity identity,Link link,String attIdentity,String attAccount) {
		
		String isImportedFromSAP=identity.getStringAttribute("isImportedFromSAP");
		if(isImportedFromSAP == null || isImportedFromSAP.equals("") || !isImportedFromSAP.equalsIgnoreCase("EXT")) {
			 return link.getAttribute(attAccount);
		}else {
		       return identity.getAttribute(attIdentity);
		}
		
	}
	
	public static Object getSapAttributeValue(Identity identity,Link link,String attIdentity,String calculedValue) {
		
		String isImportedFromSAP=identity.getStringAttribute("isImportedFromSAP");
		if (isImportedFromSAP == null || isImportedFromSAP.equals("") || !isImportedFromSAP.equalsIgnoreCase("EXT")){
		    return calculedValue;
		}
		else{
		    return identity.getAttribute(attIdentity);
		}
		
	}
	
	public static String UpdateDate_ForSQL(String dateIn) {
		
	/*	String dateOut ="";
		if ( dateIn != null && dateIn != "") 
		{
			//String format = " 00:00:00.000";
			dateOut = dateIn;
		}
		
		if ( dateIn == null || dateIn == "") 
		{
			dateOut = dateIn;
		}
	*/
		return dateIn;
	}
	
	/* Calcul Attribu HasBadge
	 * */
		
	public static String Get_HasBadge (String localisation) 
	{
		String hasBage ="O";
		
		if (localisation != null && localisation != "") 
		{
	 
			if (localisation.compareTo("Inde")==0 || localisation.compareTo("INDE")==0 || localisation.compareTo("Extern")==0 || localisation.compareTo("EXTERN")==0) 
			{
				hasBage ="N"; 
			}
	 
		}
		return hasBage;
	}
	
	
	/* Vérification Nom et prénom nom Null
	 * */
	 
	public static String Verification_NomPrenom_NonNul (String nom, String prenom) 
	{
		String verification ="Valide";
		
		if ((nom == null && prenom == null) || (nom == "" && prenom == ""))
		{
			verification = "NonValide";
		}
		
		return verification;
	}
	
	/* * Calcul de l'attribut HasAD
	 * */
	
	public static String Calcul_HasAD_Rhodes (String codePopulation) 
	{
		String hasAD ="N";
		codePopulation = codePopulation.toUpperCase();
		
		if (codePopulation != null && codePopulation !="") 
		{
			codePopulation = codePopulation.replace(" ", "");
			if (codePopulation.compareTo("G")==0 || codePopulation.compareTo("AC")==0 || codePopulation.compareTo("L")==0)
			{
			hasAD ="N";
			}
		
			else 
			{
			hasAD ="O";
			}
		}
		return hasAD;
	}
	
	
	/* * Génération Matricule Rhodes Temporaire
	 * */
	
	public static String Get_MatriculeRhodes_Temporaire () 
	{
		String matriculeTemp ="" ;
			
		Random random = new Random();
		int nb;
		int pas;
			
		do 
		{
			pas = random.nextInt(10000);
			nb = random.nextInt(100);
		} while (nb == 0);
			
		matriculeTemp = ""+nb;
		int long_matriculeTemp = matriculeTemp.length();
		
		if (long_matriculeTemp == 1) 
		{
			nb = (nb * 100000) + pas;
			matriculeTemp ="ZZ" + nb;
		}
		else 
		{
			nb = (nb * 10000) + pas;
			matriculeTemp ="ZZ" + nb;
		}
			
		System.out.println("Matricule Rhodes Temporaire généré : " + matriculeTemp   );
		
		return matriculeTemp ;
		
	}
	
	public static String Check_Date (String dateFinValidite) 
	{
		
		if (dateFinValidite != null && dateFinValidite.compareTo("2079-12-31")==0)
		{
			dateFinValidite = null;
		}
		
		return dateFinValidite;
	}
	
	public static String TraiteChaineForAeos(String chaine)
	{
		String str;
		
		if(chaine == null) 
		{
			str = "";
		}
		else
		{
		str = chaine;
		
		str = str.replace("À","A");
		str = str.replace("Á","A");
		str = str.replace("Â","A");
		str = str.replace("Ã","A");
		str = str.replace("Ä","A");
		str = str.replace("Å","A");
		str = str.replace("Æ","AE");
		str = str.replace("Ç","C");
		str = str.replace("È","E");
		str = str.replace("É","E");
		str = str.replace("Ê","E");
		str = str.replace("Ë","E");
		str = str.replace("Ì","I");
		str = str.replace("Í","I");
		str = str.replace("Î","I");
		str = str.replace("Ï","I");
		str = str.replace("Ñ","N");
		str = str.replace("Ò","O");
		str = str.replace("Ó","O");
		str = str.replace("Ô","O");
		str = str.replace("Õ","O");
		str = str.replace("Ö","O");
		str = str.replace("Ø","O");
		str = str.replace("Ù","U");
		str = str.replace("Ú","U");
		str = str.replace("Û","U");
		str = str.replace("Ü","U");
		str = str.replace("Ý","Y");
		str = str.replace("à","a");
		str = str.replace("á","a");
		str = str.replace("â","a");
		str = str.replace("ã","a");
		str = str.replace("ä","a");
		str = str.replace("å","a");
		str = str.replace("æ","ae");
		str = str.replace("ç","c");
		str = str.replace("è","e");
		str = str.replace("é","e");
		str = str.replace("ê","e");
		str = str.replace("ë","e");
		str = str.replace("ì","i");
		str = str.replace("í","i");
		str = str.replace("î","i");
		str = str.replace("ï","i");
		str = str.replace("ñ","n");
		str = str.replace("ò","o");
		str = str.replace("ó","o");
		str = str.replace("ô","o");
		str = str.replace("õ","o");
		str = str.replace("ö","o");
		str = str.replace("ø","o");
		str = str.replace("ß","s");
		str = str.replace("ù","u");
		str = str.replace("ú","u");
		str = str.replace("û","u");
		str = str.replace("ü","u");
		str = str.replace("ý","y");
		str = str.replace("ÿ","y");
		str = str.replace("!"," ");
		str = str.replace("#"," ");
		str = str.replace("$"," ");
		str = str.replace("%"," ");
		str = str.replace("&"," ");
		str = str.replace("'"," ");
		str = str.replace("("," ");
		str = str.replace(")"," ");
		str = str.replace("*"," ");
		str = str.replace("+"," ");
		str = str.replace(":"," ");
		str = str.replace(";"," ");
		str = str.replace("<"," ");
		str = str.replace("="," ");
		str = str.replace(">"," ");
		str = str.replace("?"," ");
		str = str.replace("@"," ");
		str = str.replace("¡"," ");
		str = str.replace("¢"," ");
		str = str.replace("£"," ");
		str = str.replace("¤"," ");
		str = str.replace("¥"," ");
		str = str.replace("¦"," ");
		str = str.replace("§"," ");
		str = str.replace("¨"," ");
		str = str.replace("©"," ");
		str = str.replace("ª"," ");
		str = str.replace("«"," ");
		str = str.replace("¬"," ");
		str = str.replace("®"," ");
		str = str.replace("¯"," ");
		str = str.replace("°"," ");
		str = str.replace("±"," ");
		str = str.replace("²"," ");
		str = str.replace("³"," ");
		str = str.replace("´"," ");
		str = str.replace("µ"," ");
		str = str.replace("¶"," ");
		str = str.replace("·"," ");
		str = str.replace("¸"," ");
		str = str.replace("¹"," ");
		str = str.replace("º"," ");
		str = str.replace("»"," ");
		str = str.replace("¼"," ");
		str = str.replace("½"," ");
		str = str.replace("¾"," ");
		str = str.replace("¿"," ");
		str = str.replace("`"," ");
		
		}

		return str;
	}
}