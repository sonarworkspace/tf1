package idento.semaphore.ws;

public class Aeos {
	public static String attributAeos(String baliseName,String attributValue) {

		if (attributValue !=null && attributValue !=""){
			return "<sch:"+baliseName+">"+idento.semaphore.regles.Traitement.TraiteChaineForAeos(attributValue)+"</sch:"+baliseName+">";
		}else{
			return "";
		}
	}


	public static String dateAeos(String baliseName,String attributValueDate) {

		if (attributValueDate !=null && attributValueDate !=""){
			return "<sch:"+baliseName+">"+attributValueDate+"T00:00:00"+"</sch:"+baliseName+">";
		}else{
			return "";
		}
	}

	public static String freeFieldAeos(String id,String attributValuefreeField) {
	    attributValuefreeField=attributValuefreeField!=null?attributValuefreeField:"";
		    //    if (attributValuefreeField !=null && attributValuefreeField !="" && id!=""){
        return "<sch:Freefield>" +
            "<sch:DefinitionId>"+id+"</sch:DefinitionId>" +
			"<sch:value>"+idento.semaphore.regles.Traitement.TraiteChaineForAeos(attributValuefreeField)+ "</sch:value>"+ 
            "</sch:Freefield>";

	}
}

