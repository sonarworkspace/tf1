package idento.semaphore.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import idento.semaphore.ws.model.CreateError;
import idento.semaphore.ws.model.CreateIdentityRequest;
import sailpoint.api.Workflower;
import sailpoint.object.ProvisioningPlan;
import sailpoint.object.ProvisioningPlan.AccountRequest;
import sailpoint.object.ProvisioningPlan.AttributeRequest;
import sailpoint.object.ProvisioningPlan.ObjectOperation;
import sailpoint.object.ProvisioningPlan.Operation;
import sailpoint.object.QueryOptions;
import sailpoint.object.Attributes;
import sailpoint.object.Filter;
import sailpoint.object.Identity;
import sailpoint.object.Workflow;
import sailpoint.object.WorkflowLaunch;
import sailpoint.rest.BaseResource;
import sailpoint.tools.GeneralException;
import  idento.semaphore.regles.*;
// definition_du_path de_la_classe : url_du_web service = url_didentityiq/path_de_la_classe/path_de_la_methode
@Path("semaphore")
public class CreateUser extends BaseResource {


	private AttributeRequest att(String attribute, String value) {
		AttributeRequest attributeRequest = new AttributeRequest();
		attributeRequest.setOperation(ProvisioningPlan.Operation.Set);
		attributeRequest.setName(attribute);
		attributeRequest.setValue(value);
		return attributeRequest;
	}

	private AttributeRequest att2(String attribute, HashMap<String, Object> parameters) {
		AttributeRequest attributeRequest = new AttributeRequest();
		attributeRequest.setOperation(ProvisioningPlan.Operation.Set);
		attributeRequest.setName(attribute);
		attributeRequest.setValue(parameters.get(attribute));
		return attributeRequest;
	}
	
	public boolean isDateValid(String attribut, HashMap<String, Object> body) {
		String date=(String) body.get(attribut);
		if (date==null|| date.equals("")) {
			return true;
		}
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd");
			LocalDate.parse(date, dtf.withResolverStyle(ResolverStyle.STRICT));
		return true;
		} catch (Exception e) {
			return false;
		}
	}
	public static boolean isDateValid2(String date) {
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd");
			LocalDate.parse(date, dtf.withResolverStyle(ResolverStyle.STRICT));
			return true;
		} catch (DateTimeParseException e) {
			e.printStackTrace();
			return false;
		}
	}

	/*public static void main(String ...arg) {
		System.out.println(isDateValid2("2030-02-12"));
	}*/
	private Response dateErrorResponse(String attribut) {
		return Response.status(Status.BAD_REQUEST)
				.entity(new CreateError("400", "Format de "+ attribut +" incorrect, format attendu: yyyy-MM-dd"))
				.build();
	}

	// Definir le chemin pour appeller le web service
	@POST
	// path_de_la_methode
	@Path("users")
	// Definition de la methode de creation d'une identite
	public Response createIdentity(HashMap<String, Object> body) {

		if(!isDateValid("dateDeNaissance", body)) {
			return dateErrorResponse("dateDeNaissance");
		}
		
		if(!isDateValid("dateDebutValidite", body)) {
			return dateErrorResponse("dateDebutValidite");
		}
		
		if(!isDateValid("dateFinValidite", body)) {
			return dateErrorResponse("dateFinValidite");
		}
		
		if(!isDateValid("dateDebutSuspension", body)) {
			return dateErrorResponse("dateDebutSuspension");
		}	
		if(!isDateValid("dateDebutBadge", body)) {
			return dateErrorResponse("dateDebutBadge");
		}
				
		if(!isDateValid("dateFinBadge", body)) {
			return dateErrorResponse("dateFinBadge");
		}
			
	
		if(!isDateValid("dateFinSuspension", body)) {
			return dateErrorResponse("dateFinSuspension");
		}
		
		try {
			Identity id = new Identity();
			
			if(body.get("manager")!=null && !body.get("manager").equals("")) {
				Identity manager = (Identity) getContext().getObjectByName(Identity.class, (String) body.get("manager"));	
				if (manager == null) {
					
					return Response.status(Status.BAD_REQUEST)
							.entity(new CreateError("400", "L'identifiant du manager n'exite pas dans IIQ"))
							.build();
					
				}else {
					
					id.setManager(manager);
				}
			}
			
			
			QueryOptions queryOption = new QueryOptions();

			queryOption.addFilter(Filter.eq("idSNOW",(String)body.get("idSNOW")));
			
			//queryOption.addFilter(Filter.ignoreCase(Filter.eq("firstname", fName)));

			Iterator identityIterator =getContext().search(Identity.class, queryOption); 
			if(identityIterator!=null && identityIterator.hasNext()) {
				return Response.status(Status.CONFLICT)
						.entity(new CreateError("409", "Une identite avec ce idSNow existe deja"))
						.build();
			}
			

			
			
				
			//Identity id = new Identity();
			String name = "";
			do {
				String uiid = UUID.randomUUID().toString().toUpperCase();
				
				
				// Recuperation d'une idente avec son guid
				Identity identity = (Identity) getContext().getObjectByName(Identity.class, uiid);
				if (identity == null) {
					name = uiid;
				}

			} while (name.equalsIgnoreCase(""));
			
			
			String lastname = (String) body.get("lastname");
			String firstname= (String) body.get("firstname");
			String dateDeNaissance= (String) body.get("dateDeNaissance");
			
			id.setName(name);
			id.needsCreateProcessing(Identity.CreateProcessingStep.Trigger);
			getContext().saveObject(id);
			getContext().commitTransaction();

			AccountRequest accountRequest = new AccountRequest();
			ProvisioningPlan spPlan = new ProvisioningPlan();
			accountRequest.setApplication("IIQ");
			accountRequest.setNativeIdentity(name);
			accountRequest.setOperation(ProvisioningPlan.AccountRequest.Operation.Create);
			accountRequest.add(att("empreinte", Traitement.Get_Empreinte(lastname, firstname, dateDeNaissance)));
			accountRequest.add(att2("civilite", body));
			accountRequest.add(att("firstname", Traitement.uppercaseFirstLetter(firstname)));
			accountRequest.add(att2("initialesPrenoms", body));
			accountRequest.add(att("lastname", Traitement.uppercaseletter(lastname)));
			accountRequest.add(att2("initialesOfficielles", body));
			accountRequest.add(att2("nomJeuneFille", body));
			accountRequest.add(att2("pseudonyme", body));
			accountRequest.add(att2("dateDeNaissance", body));
			accountRequest.add(att2("codeSociete", body));
			accountRequest.add(att2("libelleSociete", body));
			accountRequest.add(att2("codeSocietePrestataire", body));
			accountRequest.add(att2("libelleSocietePrestataire", body));
			accountRequest.add(att2("dateDebutValidite", body));
			accountRequest.add(att2("dateFinValidite", body));
			accountRequest.add(att2("dateDebutSuspension", body));
			accountRequest.add(att2("dateFinSuspension", body));
			accountRequest.add(att2("codePopulation", body));
			accountRequest.add(att2("libellePopulation", body));
			accountRequest.add(att2("codeContrat", body));
			accountRequest.add(att2("libelleContrat", body));
			accountRequest.add(att2("codeDirection", body));
			accountRequest.add(att2("libelleDirection", body));
			accountRequest.add(att2("codeDomaineFonctionnel", body));
			accountRequest.add(att2("codeCentreCout", body));
			accountRequest.add(att2("codeAffectation", body));
			accountRequest.add(att2("libelleAffectation", body));
			accountRequest.add(att2("localisation", body));
			accountRequest.add(att2("isPlanifie", body));
			accountRequest.add(att2("isManager", body));
			accountRequest.add(att2("manager", body));
			accountRequest.add(att2("idSAP", body));
			accountRequest.add(att2("email", body));
			accountRequest.add(att2("loginAD", body));
			accountRequest.add(att("statut", "Actif"));
			accountRequest.add(att2("codeClassification", body));
			accountRequest.add(att2("codeIndice", body));
			accountRequest.add(att2("codeNiveauResponsabilite", body));
			accountRequest.add(att2("codeRegime", body));
			accountRequest.add(att2("dateAncienneteReconnue", body));
			accountRequest.add(att2("dateEntreeGroupe", body));
			accountRequest.add(att2("fax", body));
			accountRequest.add(att("guid", name));
			accountRequest.add(att2("hasCarteDePresse", body));
			accountRequest.add(att2("idSociete", body));
			accountRequest.add(att2("idUniteComptable", body));
			accountRequest.add(att2("isActionSociale", body));
			accountRequest.add(att2("isCreationUrgente", body));
			accountRequest.add(att2("isEpargneSalariale", body));
			accountRequest.add(att2("isFormation", body));
			accountRequest.add(att2("isImportedFromSNow", body));
			accountRequest.add(att2("isImportedFromRhodes", body));
			accountRequest.add(att2("isImportedFromSAP", body));
			accountRequest.add(att2("isInCoDG", body));
			accountRequest.add(att2("isInCoDir", body));
			accountRequest.add(att2("isInCoMgt", body));
			accountRequest.add(att2("isInCoMgtElargi", body));
			accountRequest.add(att2("isMobilite", body));
			accountRequest.add(att2("isOnListeRouge", body));
			accountRequest.add(att2("isPaie", body));
			accountRequest.add(att2("isRH", body));
			accountRequest.add(att2("libelleDomaine", body));
			accountRequest.add(att2("libelleFiliere", body));
			accountRequest.add(att2("libelleFonction", body));
			accountRequest.add(att2("libelleNiveauResponsabilite", body));
			accountRequest.add(att2("nomTraite", body));
			accountRequest.add(att2("numeroCartePresse", body));
			accountRequest.add(att2("prenomTraite", body));
			accountRequest.add(att2("telephoneAssistante", body));
			accountRequest.add(att2("telephoneFixe", body));
			accountRequest.add(att2("telephonePortable", body));
			accountRequest.add(att2("trancheDeDecompte", body));
			accountRequest.add(att2("hasAD", body));
			accountRequest.add(att2("hasBadge", body));
			accountRequest.add(att2("dateDebutBadge", body));
			accountRequest.add(att2("dateFinBadge", body));
			accountRequest.add(att2("paysAffectation", body));
			accountRequest.add(att2("villeAffectation", body));
			accountRequest.add(att2("referent", body));
			accountRequest.add(att2("batiment", body));
			accountRequest.add(att2("etage", body));
			accountRequest.add(att2("alveole", body));
			accountRequest.add(att2("idRhodes", body));
			accountRequest.add(att2("codePopulationRhodes", body));
			accountRequest.add(att2("libellePopulationRhodes", body));
			accountRequest.add(att2("codeImputationRhodes", body));
			accountRequest.add(att2("numeroBadge", body));
			accountRequest.add(att2("codeSite", body));
			accountRequest.add(att2("compteurMAJRhodes", body));
			accountRequest.add(att2("hasParking", body));
			accountRequest.add(att("guidFromAD", "{" + name + "}"));
			accountRequest.add(att2("isDoublon", body));
			accountRequest.add(att2("referenceContrat", body));
			accountRequest.add(att2("pieceIdentite", body));
			accountRequest.add(att2("idSNOW", body));

			spPlan.add(accountRequest);
			spPlan.setIdentity(id);

			String caseName = "LCM Create and Update";
			String workflowName = "LCM Create and Update";
			String requesterId = "spadmin";

			Map args = new HashMap();
			Workflow eventWorkflow = getContext().getObject(Workflow.class, workflowName);
			System.out.println("eventWorkflow : ." + eventWorkflow);
			if (null == eventWorkflow) {
				System.out.println("Could not find a workflow named: " + workflowName);
				throw new GeneralException("Invalid worklfow: " + workflowName);
			}
			System.out.println("requesterId : ." + requesterId);
			// Simulate the request being submitted by a user. Default: spadmin.
			// Identity id = getContext().getObject(Identity.class, "spadmin");
			// log.debug("id : ."+id);
			/*
			 * if (null == id) { System.out.println("Could not find a requester Identity: "
			 * + requesterId); throw new GeneralException("Invalid identity: " +
			 * requesterId); }
			 */
			// Ask the Request Processor to start the workflow 5 seconds from now.
			// Append the time stamp to the workflow case name to ensure it's unique.
			long launchTime = System.currentTimeMillis() + 50000;
			caseName = caseName + "(" + launchTime + ")";
			// Build out a map of arguments to pass to the Request Scheduler.
			Attributes reqArgs = new Attributes();

			Attributes wfArgs = new Attributes();
			wfArgs.put("requester", requesterId);
			wfArgs.put("launcher", requesterId);
			wfArgs.put("userName", name);
			wfArgs.put("identityName", name);
			wfArgs.put("plan", spPlan);
			wfArgs.put("flow", "IdentityCreateRequest");
			wfArgs.put("source", "LCM");
			wfArgs.put("approvalScheme", "none");
			//Lancement de la refresh  
			wfArgs.put("doRefresh", "true");

			reqArgs.putAll(wfArgs);

			//
			// Launch Workflow
			//
			WorkflowLaunch wflaunch = new WorkflowLaunch();
			wflaunch.setWorkflowName(eventWorkflow.getName());
			wflaunch.setWorkflowRef(eventWorkflow.getName());
			wflaunch.setCaseName(caseName);
			wflaunch.setVariables(reqArgs);
			wflaunch.setLauncher(requesterId);
			Workflower workflower = new Workflower(getContext());

			WorkflowLaunch launch = workflower.launch(wflaunch);

		} catch (GeneralException e) {
			CreateError createError = new CreateError();
			try {
				getContext().rollbackTransaction();
			} catch (GeneralException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				createError.setDetail(e.getMessage());
				createError.setStatus("500");
			    return	Response.status(Status.INTERNAL_SERVER_ERROR).entity(createError).build();
			}
			e.printStackTrace();
			createError.setDetail(e.getMessage());
			createError.setStatus("500");
		    return	Response.status(Status.INTERNAL_SERVER_ERROR).entity(createError).build();
			
		}

	return Response.ok().build();

	}

}
