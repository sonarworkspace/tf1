package idento.semaphore.ws;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import sailpoint.tools.Message;
import sailpoint.object.Field;
import java.util.Map;

import com.rsa.authmgr.admin.groupmgt.GetTimeRestrictedAccessCommand;

import java.util.Calendar;



               

public class DateTransform {
	
	public static String iiqToSnowDate(String iiqDate) {
		String snowDate = "";
		if(iiqDate!=null && !iiqDate.equals("")) {			
			SimpleDateFormat iqSdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat snowSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
			try {
				Date date=iqSdf.parse(iiqDate);
				snowDate=snowSdf.format(date);
				
			} catch (ParseException e) {
					e.printStackTrace();	
			}
		}
		return snowDate;	
	}
	
	/**
	 * Transforme une date SNOW de format dd/MM/yyyy en date au format yyyy-MM-dd
	 * @param snowDate La date SNOW
	 * @return la date au format IIQ
	 */
	public static String snowToIqDate(String snowDate) {
		String iiqDate = "";
		if(snowDate!=null && !snowDate.equals("")) {			
			SimpleDateFormat iqSdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat snowSdf = new SimpleDateFormat("dd/MM/yyyy");
	
			try {
				Date date=snowSdf.parse(snowDate);
				iiqDate=iqSdf.format(date);
				
			} catch (ParseException e) {
					e.printStackTrace();	
			}
		}
		return iiqDate;	
	}
	

}