package idento.semaphore.ws;

import sailpoint.rest.SailPointRestApplication;

public class IdentoWsApplication extends SailPointRestApplication{
	
	public IdentoWsApplication() {
		super();
		register(UpdateUser.class);
		register(CreateUser.class);
	}

}
