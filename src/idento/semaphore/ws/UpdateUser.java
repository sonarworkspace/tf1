package idento.semaphore.ws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import idento.semaphore.regles.Traitement;
import idento.semaphore.ws.model.CreateError;
import sailpoint.api.SailPointContext;
import sailpoint.api.Workflower;
import sailpoint.object.Attributes;
import sailpoint.object.Identity;
import sailpoint.object.ProvisioningPlan;
import sailpoint.object.Workflow;
import sailpoint.object.WorkflowLaunch;
import sailpoint.object.ProvisioningPlan.AccountRequest;
import sailpoint.object.ProvisioningPlan.AttributeRequest;
import sailpoint.rest.BaseResource;
import sailpoint.tools.GeneralException;
import  idento.semaphore.regles.*;

@Path("semaphore")
public class UpdateUser extends BaseResource{
	private static final String FORMAT_DATE="yyyy-MM-dd";
	
	private AttributeRequest att(String attribute, Object value) {

		AttributeRequest attributeRequest = new AttributeRequest();
		attributeRequest.setOperation(ProvisioningPlan.Operation.Set);
		attributeRequest.setName(attribute);
		attributeRequest.setValue(value);
		return attributeRequest;
	}
	
	private void addc(AccountRequest accountRequest, String attribute, Object value) {
		if(value != null) {
			accountRequest.add(att(attribute,value));
		}
	}

	private AttributeRequest att2(String attribute, HashMap<String, Object> body) {
		AttributeRequest attributeRequest = new AttributeRequest();
		attributeRequest.setOperation(ProvisioningPlan.Operation.Set);
		attributeRequest.setName(attribute);
		attributeRequest.setValue(body.get(attribute));
		return attributeRequest;
	}
	
	private void addc2(AccountRequest accountRequest, String attribute, HashMap<String, Object> body) {
		addc(accountRequest, attribute, body.get(attribute));
	}
	// Gestion du format de la date 
	public boolean isDateValid(String attribut, HashMap<String, Object> body) {
		String date=(String) body.get(attribut);
		if (date==null|| date.equals("")) {
			return true;
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
			sdf.parse((String)body.get(attribut));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	private Response dateErrorResponse(String attribut) {
		return Response.status(Status.BAD_REQUEST)
				.entity(new CreateError("400", "Format de "+ attribut +" incorrect, format attendu: "+FORMAT_DATE))
				.build();
	}

	// Definir le chemin pour appeller le web service
	@PUT
	@Path("users/{uiid}")
	// Definition de la methode de creation d'une identite

	public Response updateIdentity(HashMap<String, Object> body,@PathParam("uiid") String uiid) {
		if(!isDateValid("dateDeNaissance", body)) {
			return dateErrorResponse("dateDeNaissance");
		}
		
		if(!isDateValid("dateDebutValidite", body)) {
			return dateErrorResponse("dateDebutValidite");
		}
		
		if(!isDateValid("dateFinValidite", body)) {
			return dateErrorResponse("dateFinValidite");
		}
		if(!isDateValid("dateDebutBadge", body)) {
			return dateErrorResponse("dateDebutBadge");
		}
				
		if(!isDateValid("dateFinBadge", body)) {
			return dateErrorResponse("dateFinBadge");
		}
		if(!isDateValid("dateDebutSuspension", body)) {
			return dateErrorResponse("dateDebutSuspension");
		}
		
		if(!isDateValid("dateFinSuspension", body)) {
			return dateErrorResponse("dateFinSuspension");
		}

		try {
			
			Identity id = (Identity) getContext().getObjectByName(Identity.class, uiid);
			if(id == null) {
				CreateError error= new CreateError();
				error.setStatus("404");
				error.setDetail("Impossible de trouver l'idendtite: <"+ uiid+">");
				return Response.status(Status.NOT_FOUND).entity(error).build();
			}
			if(body.get("manager")!=null && !body.get("manager").equals("")) {
				Identity manager = (Identity) getContext().getObjectByName(Identity.class, (String) body.get("manager"));	
				if (manager == null) {
					
					return Response.status(Status.BAD_REQUEST)
							.entity(new CreateError("400", "L'identifiant du manager n'exite pas dans IIQ"))
							.build();
					
				}else {
					
					id.setManager(manager);
				}
			}	
			
			String statut="Inatif";
			String dateFinValidite=(String) body.get("dateFinValidite");
			if (dateFinValidite==null || dateFinValidite.equals("")) {
				
				statut = "Actif";
			}else {
				SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
				Date dateFinValiditeDate;
				
				try {
					//Date datejour=new Date();
				
					Calendar datejour= Calendar.getInstance();
					datejour.add(Calendar.DAY_OF_MONTH,-90);
					dateFinValiditeDate = sdf.parse(dateFinValidite);					
					if (dateFinValiditeDate.after(datejour.getTime())){
						statut = "Actif";
					}else{ 
							
						statut = "Inatif"; 
					}	
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			String lastname = (String) body.get("lastname");
			String firstname= (String) body.get("firstname");
			
			
			//if(body.get("dateFinValidite")==null && !body.get("dateFinValidite").equals(""))
				
				
							
			//id.needsCreateProcessing(Identity.CreateProcessingStep.Trigger);

			AccountRequest accountRequest = new AccountRequest();
			ProvisioningPlan spPlan = new ProvisioningPlan();
			accountRequest.setApplication("IIQ");
			accountRequest.setNativeIdentity(uiid);
			accountRequest.setOperation(ProvisioningPlan.AccountRequest.Operation.Modify);
			if (lastname!=null) {
				accountRequest.add(att("lastname", Traitement.uppercaseletter(lastname)));
			}
			if (firstname!=null) {
				accountRequest.add(att("firstname", Traitement.uppercaseFirstLetter(firstname)));
			}
		
			addc2(accountRequest,"initialesPrenoms", body);
			addc2(accountRequest,"civilite", body);
			addc2(accountRequest,"initialesOfficielles", body);
			addc2(accountRequest,"nomJeuneFille", body);
			addc2(accountRequest,"pseudonyme", body);
			addc2(accountRequest,"dateDeNaissance", body);
			addc2(accountRequest,"codeSociete", body);
			addc2(accountRequest,"libelleSociete", body);
			addc2(accountRequest,"codeSocietePrestataire", body);
			addc2(accountRequest,"libelleSocietePrestataire", body);
			addc2(accountRequest,"dateDebutValidite", body);
			addc2(accountRequest,"dateFinValidite", body);
			addc2(accountRequest,"dateDebutSuspension", body);
			addc2(accountRequest,"dateFinSuspension", body);
			addc2(accountRequest,"codePopulation", body);
			addc2(accountRequest,"libellePopulation", body);
			addc2(accountRequest,"codeContrat", body);
			addc2(accountRequest,"libelleContrat", body);
			addc2(accountRequest,"codeDirection", body);
			addc2(accountRequest,"libelleDirection", body);
			addc2(accountRequest,"codeDomaineFonctionnel", body);
			addc2(accountRequest,"codeCentreCout", body);
			addc2(accountRequest,"codeAffectation", body);
			addc2(accountRequest,"libelleAffectation", body);
			addc2(accountRequest,"localisation", body);
			addc2(accountRequest,"isPlanifie", body);
			addc2(accountRequest,"isManager", body);
			addc2(accountRequest,"manager", body);
			addc2(accountRequest,"idSAP", body);
			addc2(accountRequest,"email", body);
			addc2(accountRequest,"loginAD", body);
			addc2(accountRequest,"codeClassification", body);
			
			accountRequest.add(att("statut", statut));
			System.out.println("Le statut est:"+statut);
			addc2(accountRequest,"codeIndice", body);
			addc2(accountRequest,"codeNiveauResponsabilite", body);
			addc2(accountRequest,"codeRegime", body);
			addc2(accountRequest,"dateAncienneteReconnue", body);
			addc2(accountRequest,"dateEntreeGroupe", body);
			addc2(accountRequest,"fax", body);
			addc2(accountRequest,"hasCarteDePresse", body);
			accountRequest.add(att("guid", uiid));
			addc2(accountRequest,"hasCarteDePresse", body);
			addc2(accountRequest,"idSociete", body);
			addc2(accountRequest,"idUniteComptable", body);
			addc2(accountRequest,"isActionSociale", body);
			addc2(accountRequest,"isCreationUrgente", body);
			addc2(accountRequest,"isEpargneSalariale", body);
			addc2(accountRequest,"isFormation", body);
			addc2(accountRequest,"isImportedFromSNow", body);
			addc2(accountRequest,"isImportedFromRhodes", body);
			addc2(accountRequest,"isImportedFromSAP", body);
			addc2(accountRequest,"isInCoDG", body);
			addc2(accountRequest,"isInCoDir", body);
			addc2(accountRequest,"isInCoMgt", body);
			addc2(accountRequest,"isInCoMgtElargi", body);
			addc2(accountRequest,"isMobilite", body);
			addc2(accountRequest,"isOnListeRouge", body);
			addc2(accountRequest,"isPaie", body);
			addc2(accountRequest,"isRH", body);
			addc2(accountRequest,"libelleDomaine", body);
			addc2(accountRequest,"libelleFiliere", body);
			addc2(accountRequest,"libelleFonction", body);
			addc2(accountRequest,"libelleNiveauResponsabilite", body);
			addc2(accountRequest,"nomTraite", body);
			addc2(accountRequest,"numeroCartePresse", body);
			addc2(accountRequest,"prenomTraite", body);
			addc2(accountRequest,"telephoneAssistante", body);
			addc2(accountRequest,"telephoneFixe", body);
			addc2(accountRequest,"telephonePortable", body);
			addc2(accountRequest,"trancheDeDecompte", body);
			addc2(accountRequest,"hasAD", body);
			addc2(accountRequest,"hasBadge", body);
			addc2(accountRequest,"dateDebutBadge", body);
			addc2(accountRequest,"dateFinBadge", body);
			addc2(accountRequest,"paysAffectation", body);
			addc2(accountRequest,"villeAffectation", body);
			addc2(accountRequest,"referent", body);
			addc2(accountRequest,"batiment", body);
			addc2(accountRequest,"etage", body);
			addc2(accountRequest,"alveole", body);
			addc2(accountRequest,"idRhodes", body);
			addc2(accountRequest,"codePopulationRhodes", body);
			addc2(accountRequest,"libellePopulationRhodes", body);
			addc2(accountRequest,"codeImputationRhodes", body);
			addc2(accountRequest,"numeroBadge", body);
			addc2(accountRequest,"codeSite", body);
			addc2(accountRequest,"compteurMAJRhodes", body);
			addc2(accountRequest,"hasParking", body);
			accountRequest.add(att("guidFromAD","{"+uiid+"}"));
			addc2(accountRequest,"isDoublon", body);
			addc2(accountRequest,"referenceContrat", body);
			addc2(accountRequest,"pieceIdentite", body);
			addc2(accountRequest,"idSNOW", body);
			addc2(accountRequest,"csnAEOS", body);
			

			spPlan.add(accountRequest);
			spPlan.setIdentity(id);

			String caseName = "LCM Create and Update";
			String workflowName = "LCM Create and Update";
			String requesterId = "spadmin";

			Map args = new HashMap();
			Workflow eventWorkflow = getContext().getObject(Workflow.class, workflowName);
			System.out.println("eventWorkflow : ." + eventWorkflow);
			if (null == eventWorkflow) {
				System.out.println("Could not find a workflow named: " + workflowName);
				throw new GeneralException("Invalid worklfow: " + workflowName);
			}
			System.out.println("requesterId : ." + requesterId);
			// Simulate the request being submitted by a user. Default: spadmin.
			// Identity id = getContext().getObject(Identity.class, "spadmin");
			// log.debug("id : ."+id);
			/*
			 * if (null == id) { System.out.println("Could not find a requester Identity: "
			 * + requesterId); throw new GeneralException("Invalid identity: " +
			 * requesterId); }
			 */
			// Ask the Request Processor to start the workflow 5 seconds from now.
			// Append the time stamp to the workflow case name to ensure it's unique.
			long launchTime = System.currentTimeMillis() + 50000;
			caseName = caseName + "(" + launchTime + ")";
			// Build out a map of arguments to pass to the Request Scheduler.
			Attributes reqArgs = new Attributes();

			Attributes wfArgs = new Attributes();
			wfArgs.put("requester", requesterId);
			wfArgs.put("launcher", requesterId);
			wfArgs.put("userName", uiid);
			wfArgs.put("identityName", uiid);
			wfArgs.put("plan", spPlan);
			wfArgs.put("flow", "IdentityCreateRequest");
			wfArgs.put("source", "LCM");
			wfArgs.put("approvalScheme", "none");
			//Lancement de la refresh  
			wfArgs.put("doRefresh", "true");

			reqArgs.putAll(wfArgs);

			//
			// Launch Workflow
			//
			WorkflowLaunch wflaunch = new WorkflowLaunch();
			wflaunch.setWorkflowName(eventWorkflow.getName());
			wflaunch.setWorkflowRef(eventWorkflow.getName());
			wflaunch.setCaseName(caseName);
			wflaunch.setVariables(reqArgs);
			wflaunch.setLauncher(requesterId);
			Workflower workflower = new Workflower(getContext());

			WorkflowLaunch launch = workflower.launch(wflaunch);
			System.out.println("WF STATUS : " + launch.getStatus());
			System.out.println("WF COMPLETE : " + launch.isComplete()+
					", WF APPROVE : " + launch.isApproving()+
					", WF FAILED : " + launch.isFailed() +
					", WF EXEC : " + launch.isExecuting());

		} catch (GeneralException e) {
			CreateError createError= new CreateError();
			// String schemas =null ;
			//createError.setSchemas(null);
			// String detail = e.getMessage();
			createError.setDetail(e.getMessage());
			createError.setStatus("500");
			//String scimType ="Erreur interne du serveur" ;
			 //createError.setScimType(scimType);
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(createError).build();
			
		}
		

		return Response.ok().build();

	}


}
