package idento.semaphore.ws.model;

public class CreateError {
	
	//private String schemas;
	private String detail;
	private String status;
	//private String scimType;
	
	public CreateError() {
	}
	
	public CreateError(String status, String detail) {
		this.status = status;
		this.detail = detail;
	}
	
	
	//Getters et Setters
	
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
