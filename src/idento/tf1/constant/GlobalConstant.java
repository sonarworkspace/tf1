package idento.tf1.constant;

public class GlobalConstant {

	/* General */
	public final static String ENCODING_UTF8 = "UTF8";
	public final static String SPLIT_POINTVIRGULE = ";";
	public final static String IDENTITY_ATTRIBUTE_NO_MANAGER = "NO_MANAGER";
	public final static String VALUE_YES = "yes";
	
	/* Custom Configuration */
	public final static String CONFIG_ALPS = "ALPSConfiguration";
	public final static String CONFIG_AD = "ADConfiguration";
	public final static String CONFIG_FLAG_LEAVER_AD = "FlagLeaverOnADConfiguration";
	public final static String CONFIG_IDENTITY_ARRIVAL = "InternalContractorArrivalConfiguration";
	public final static String CONFIG_KPI_ANALYSIS = "KPIConfiguration";
	public final static String CONFIG_IDENTITY_LEAVER = "LeaverConfiguration";
	public final static String CONFIG_SAML = "SAMLConfiguration";
	public final static String CONFIG_SYSTEM = "SystemConfiguration";
	
	/* Application */
	public final static String RH_APPLICATION = "RH";
	public final static String AD_APPLICATION = "AD";
	public final static String LDAP_APPLICATION = "LDAP";
	
	/* Identity type */
	public final static String IDENTITY_TYPE_CONTRACTOR = "CONTRACTOR";
	public final static String IDENTITY_TYPE_INTERNAL = "INTERNAL";
	public final static String IDENTITY_TYPE_INTERNAL_EMERGENCY = "ARRIVEE URGENTE";
	
	
	/* TYPE AUDIT EVENT */
	public final static String AUDIT_TYPE_EVENT_ACTION = "action";
	
	/* Status */
	public final static String IDENTITY_STATUS_ACTIVE = "ACTIVE";
	public final static String IDENTITY_STATUS_INACTIVE = "INACTIVE";
	
	/* displayname "status" attrbut */
	public final static String RH_ATTRIBUT_STATUS = "STATUS";
	public final static String AD_ATTRIBUT_STATUS = "userAccountControl";
	public final static String LDAP_ATTRIBUT_STATUS = "disabled";
	
	/* Displayname Identity attributes */
	public final static String IDENTITY_ATTRIBUTE_idmap = "tf1IdMap";
	public final static String IDENTITY_ATTRIBUTE_iddiva = "tf1IdDiva";
	public final static String IDENTITY_ATTRIBUTE_idsap = "tf1IdSAP";
	public final static String IDENTITY_ATTRIBUTE_Gender = "tf1Genre";
	public final static String IDENTITY_ATTRIBUTE_initiale_firstname = "tf1InitialePrenom";
	//public final static String IDENTITY_ATTRIBUTE_lastname = "tf1Nom";
	public final static String IDENTITY_ATTRIBUTE_lastname = "lastname";
	//public final static String IDENTITY_ATTRIBUTE_firstname = "tf1Prenom";
	public final static String IDENTITY_ATTRIBUTE_firstname = "firstname";
	public final static String IDENTITY_ATTRIBUTE_lastname_Girl = "tf1NomJeuneFille";
	public final static String IDENTITY_ATTRIBUTE_nickname = "tf1Pseudo";
	public final static String IDENTITY_ATTRIBUTE_birthday = "tf1DateNaissance";
	public final static String IDENTITY_ATTRIBUTE_idsociete = "tf1IdSociete";
	public final static String IDENTITY_ATTRIBUTE_direction_code = "tf1CodeDirection";
	public final static String IDENTITY_ATTRIBUTE_direction_libelle = "tf1LibelleDirection";
	public final static String IDENTITY_ATTRIBUTE_code_ci_affectation = "tf1CodeCIAffectation";
	public final static String IDENTITY_ATTRIBUTE_ciAffectation = "tf1CIAffectation";
	public final static String IDENTITY_ATTRIBUTE_cost_center = "tf1CentreCout";
	public final static String IDENTITY_ATTRIBUTE_uc = "tf1UC";
	public final static String IDENTITY_ATTRIBUTE_function = "tf1Fonction";
	public final static String IDENTITY_ATTRIBUTE_startdate_group = "tf1DateEntreeGroupe";
	public final static String IDENTITY_ATTRIBUTE_startdate_society = "tf1DateEntreeSociete";
	public final static String IDENTITY_ATTRIBUTE_enddate = "tf1DateSortie";
	public final static String IDENTITY_ATTRIBUTE_date_anciennete_reconnu = "tf1DateEncienneteReconnue";
	public final static String IDENTITY_ATTRIBUTE_contract_code = "tf1CodeContrat";
	public final static String IDENTITY_ATTRIBUTE_contract_libelle = "tf1LibelleContrat";
	public final static String IDENTITY_ATTRIBUTE_population_code = "tf1CodePopulation";
	public final static String IDENTITY_ATTRIBUTE_population_libelle = "tf1LibellePopulation";
	public final static String IDENTITY_ATTRIBUTE_population = "tf1Population";
	public final static String IDENTITY_ATTRIBUTE_responsability_level = "tf1NiveauResponsabilite";
	public final static String IDENTITY_ATTRIBUTE_responsability_level_code = "tf1CodeNiveauResponsabilite";
	public final static String IDENTITY_ATTRIBUTE_carte_presse = "tf1CartePresse";
	public final static String IDENTITY_ATTRIBUTE_cycle = "tf1Cycle";
	public final static String IDENTITY_ATTRIBUTE_red_list = "tf1ListeRouge";
	public final static String IDENTITY_ATTRIBUTE_filiere = "tf1Filiere";
	public final static String IDENTITY_ATTRIBUTE_classification = "tf1Classification";
	public final static String IDENTITY_ATTRIBUTE_id_sup_hierarchy = "tf1IdSuperieurHierarchique";
	public final static String IDENTITY_ATTRIBUTE_top_manager = "tf1TopManager";
	public final static String IDENTITY_ATTRIBUTE_affaires_sociales_action_social = "tf1AffairesSocialesActionSociale";
	public final static String IDENTITY_ATTRIBUTE_affaires_sociale_epargne_salariale = "tf1AffairesSocialesEpargneSalariale";
	public final static String IDENTITY_ATTRIBUTE_formation = "tf1Formation";
	public final static String IDENTITY_ATTRIBUTE_mobility = "tf1Mobilite";
	public final static String IDENTITY_ATTRIBUTE_rh= "tf1RH";
	public final static String IDENTITY_ATTRIBUTE_paie = "tf1Paie";
	public final static String IDENTITY_ATTRIBUTE_codgttf1 = "tf1CODGTF1";
	public final static String IDENTITY_ATTRIBUTE_codir = "tf1CODIR";
	public final static String IDENTITY_ATTRIBUTE_comgt = "tf1COMGT";
	public final static String IDENTITY_ATTRIBUTE_localisation = "tf1Localisation";
	public final static String IDENTITY_ATTRIBUTE_telephon_number= "tf1TelephoneNumber";
	public final static String IDENTITY_ATTRIBUTE_assistant_number = "tf1TelephoneAssistante";
	//public final static String IDENTITY_ATTRIBUTE_mail = "tf1Mail";
	public final static String IDENTITY_ATTRIBUTE_mail = "mail";
	public final static String IDENTITY_ATTRIBUTE_portable_number = "tf1TelephonePortable";
	public final static String IDENTITY_ATTRIBUTE_domain = "tf1Domaine";
	//public final static String IDENTITY_ATTRIBUTE_login = "tf1Login";
	public final static String IDENTITY_ATTRIBUTE_login = "name";
	public final static String IDENTITY_ATTRIBUTE_initial_officielle = "tf1InitialeOfficielle";
	public final static String IDENTITY_ATTRIBUTE_fax_number = "tf1Fax";
	public final static String IDENTITY_ATTRIBUTE_regime_code = "tf1CodeRegime";
	public final static String IDENTITY_ATTRIBUTE_number_presscard = "tf1NumeroCartePresse";
	public final static String IDENTITY_ATTRIBUTE_indice = "tf1Indice";
	public final static String IDENTITY_ATTRIBUTE_domain_fonctionel = "tf1DomaineFonctionnel";
	public final static String IDENTITY_ATTRIBUTE_comgt_elargi = "tf1COMGT_Elargi";
	public final static String IDENTITY_ATTRIBUTE_emergency_suspension_startdate = "tf1DateDebutSuspension";
	public final static String IDENTITY_ATTRIBUTE_emergency_suspension_enddate = "tf1DateFinSuspension";
}
